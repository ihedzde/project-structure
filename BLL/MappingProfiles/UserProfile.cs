using System;
using AutoMapper;
using Common.DTO.User;
using DAL.Domain.Models;

namespace BLL.MappingProfiles
{
    public class UserProfile: Profile
    {
        public UserProfile()
        {
            CreateMap<UserModel, UserDTO>();
            CreateMap<UserDTO, UserModel>();
            CreateMap<CreateUserDTO, UserDTO>()
            .ForMember(dest=>dest.RegisteredAt, src=> src.MapFrom(s=>DateTime.Now));
        }
    }
}
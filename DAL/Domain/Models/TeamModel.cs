using System;

namespace DAL.Domain.Models
{
    public class TeamModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}

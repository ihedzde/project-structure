using System.Threading.Tasks;

namespace DAL.Domain.Repositories
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}
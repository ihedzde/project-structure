using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Domain.Models;
using DAL.Domain.Repositories;

namespace DAL.Persistance.Repositories
{
    public class TeamRepository : IRepository<TeamModel>
    {
        private IList<TeamModel> _storage;
        public TeamRepository()
        {
            #region MockRepo
            _storage = new List<TeamModel>{
        new TeamModel{
            Id = 0,
            Name = "Denesik - Greenfelder",
            CreatedAt = DateTime.Parse("2019-08-25T15:48:06.062331+00:00")
        },
        new TeamModel{
            Id = 1,
            Name = "Durgan Group",
            CreatedAt = DateTime.Parse("2017-03-31T02:29:28.3740504+00:00")
        },
        new TeamModel{
            Id = 2,
            Name = "Kassulke LLC",
            CreatedAt = DateTime.Parse("2019-02-21T15:47:30.3797852+00:00")
        },
        new TeamModel{
            Id = 3,
            Name = "Harris LLC",
            CreatedAt = DateTime.Parse("2018-08-28T08:18:46.4160342+00:00")
        },
        new TeamModel{
            Id = 4,
            Name = "Mitchell Inc",
            CreatedAt = DateTime.Parse("2019-04-03T09:58:33.0178179+00:00")
        },
        new TeamModel{
            Id = 5,
            Name = "Smitham Group",
            CreatedAt = DateTime.Parse("2016-10-05T07:57:02.8427653+00:00")
        },
        new TeamModel{
            Id = 6,
            Name = "Kutch - Roberts",
            CreatedAt = DateTime.Parse("2016-10-31T05:05:15.1076578+00:00")
        },
        new TeamModel{
            Id = 7,
            Name = "Parisian Group",
            CreatedAt = DateTime.Parse("2016-07-17T01:34:55.0917082+00:00")
        },
        new TeamModel{
            Id = 8,
            Name = "Schiller Group",
            CreatedAt = DateTime.Parse("2020-10-05T01:20:14.1953926+00:00")
        },
        new TeamModel{
            Id = 9,
            Name = "Littel, Turcotte and Muller",
            CreatedAt = DateTime.Parse("2018-10-19T14:54:27.5549549+00:00")
        }

                };
            #endregion
        }
        public TeamModel Create(TeamModel data)
        {
            data.Id = _storage.LastOrDefault()?.Id+1?? 0;//Last id +1 to get nextone or 0 if it's first object in list
            _storage.Add(data);
            return data;
        }

        public TeamModel Delete(int id)
        {
            var deleteObj = _storage.FirstOrDefault(p=>p.Id == id);
            _storage.Remove(deleteObj);
            return deleteObj;
        }

        public TeamModel Read(int id)
        {
            return _storage.FirstOrDefault(p=>p.Id == id);
        }

        public IList<TeamModel> ReadAll()
        {
            return _storage;
        }

        public void Update(TeamModel data)
        {
            var oldObjIndex = _storage.IndexOf(data);
            if (oldObjIndex >= 0 ){
                _storage[oldObjIndex] = data;
            }
        }
    }
}
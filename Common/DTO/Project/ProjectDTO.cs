using System;
using System.Collections.Generic;
using Common.DTO.Task;
using Common.DTO.Team;
using Common.DTO.User;

namespace Common.DTO.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }
        public int TeamId { get; set; }
        public TeamDTO Team { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}

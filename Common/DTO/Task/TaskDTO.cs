using System;
using Common.DTO.User;

namespace Common.DTO.Task
{
    public class TaskDTO
    {
        private TaskState state;

        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public UserDTO Performer { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public TaskState State
        {
            get => state;
            set
            {
                state = value;
                if(state == TaskState.Finished){
                    FinishedAt = DateTime.Now;
                }
            }
        }

        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}

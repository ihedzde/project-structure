namespace Common.DTO.Task
{
    public class CreateTaskDTO
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public TaskState State { get; set; }
    }
}
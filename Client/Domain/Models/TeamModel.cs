using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Domain.Models
{
    public class TeamModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public IList<UserModel> Members { get; set; }

        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach(var member in Members){
                builder.AppendLine(member.ToString());
            }
            
            return $"TeamId: {Id}\n Name: {Name}\n CreatedAt{CreatedAt.ToString()}\n Members: {builder.ToString()}";
        }
    }
}

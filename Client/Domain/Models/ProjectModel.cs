using System;
using System.Collections.Generic;

namespace Client.Domain.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public UserModel Author { get; set; }
        public int TeamId { get; set; }
        public TeamModel Team { get; set; }
        public IList<TaskModel> Tasks{ get; set;}
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return $"ProjectId {Id}\n AuthorId: {AuthorId} \n"
            + $" TeamId: {TeamId}\n Name: {Name} \n"
            + $" Description: {Description}\n State {Deadline.ToString()}\n"
            + $" CreatedAt: {CreatedAt.ToString()}\n";
        }
    }
}

using System;
using Newtonsoft.Json;

namespace Client.Domain.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public override string ToString()
        {
            return $"UserId {Id}\n TeamId: {TeamId} \n"
            +$" FirstName: {FirstName}\n LastName: {LastName} \n"
            +$" Email: {Email}\n RegisteredAt {RegisteredAt.ToString()}\n"
            +$" BirthDay: {BirthDay.ToString()}";
        }
    }
}

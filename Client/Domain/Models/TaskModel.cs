using System;

namespace Client.Domain.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public UserModel Performer { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public short State { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public override string ToString()
        {
            return $"TaskId {Id}\n ProjectId: {ProjectId} \n"
            + $" PerformerId: {PerformerId}\n Name: {Name} \n"
            + $" Description: {Description}\n State {State.ToString()}\n"
            + $" CreatedAt: {CreatedAt.ToString()}\n FinishedAt {FinishedAt?.ToString()}";
        }
    }

}

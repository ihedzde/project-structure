using System;
using Client.Domain.Models;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Client.DTOs;
using System.Net.Http;
using Client.Domain.Configs;
using Newtonsoft.Json;

namespace Client.Services
{
    public class SelectServices : IDisposable
    {
        private readonly HttpClient _client;
        private readonly string _route = Routes.Select.ToString();
        public SelectServices()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Config.FetchUrlBase);
        }
        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<Dictionary<string, int>> GetTasksEntitiesByIdAsync(int userId)
        {
            var result = await _client.GetAsync(_route + "/0/?id=" + userId);
            var json = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Dictionary<string, int>>(json);
        }
        public async Task<IList<TaskModel>> GetLessThan45Async(int userId)
        {
            var result = await _client.GetAsync(_route + "/1/?id=" + userId);
            var json = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<TaskModel>>(json);
        }
        public async Task<IList<IdAndName>> FinishedIn2021Async(int userId)
        {
            var result = await _client.GetAsync(_route + "/2/?id=" + userId);
            var json = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<IdAndName>>(json);
        }
        public async Task<IList<IList<IdTeamNameMembers>>> TeamsOlder10SortedAsync()
        {
            var result = await _client.GetAsync(_route + "/3");
            var json = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<IList<IdTeamNameMembers>>>(json);
        }
        public async Task<IList<IdUserAndTasks>> UsersAlphabeticByTasksLength()
        {
            var result = await _client.GetAsync(_route + "/4");
            var json = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<IdUserAndTasks>>(json);
        }
        public async Task<UserTaskCountUnfinishedTaskCountLongestTask> ComplexUserEntity(int userId)
        {
            var result = await _client.GetAsync(_route + "/5/?id=" + userId);
            if(result.StatusCode == System.Net.HttpStatusCode.NoContent)
                throw new ArgumentException("No content");
            var json = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<UserTaskCountUnfinishedTaskCountLongestTask>(json);
        }
        public async Task<dynamic> OddlyProjectTaskUserCountMix()
        {
            var result = await _client.GetAsync(_route + "/6");
            var json = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<ProjectTaskUserCountMix>>(json);
        }
    }
}
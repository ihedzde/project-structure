using System.Collections.Generic;
using System.Reflection;
using BLL.MappingProfiles;
using BLL.Services;
using DAL.Domain.Models;
using DAL.Domain.Repositories;
using DAL.Persistance.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace WebAPI.Extenstions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddSingleton<IRepository<UserModel>, UserRepository>();
            services.AddSingleton<IRepository<TeamModel>, TeamRepository>();
            services.AddSingleton<IRepository<TaskModel>, TaskRepository>();
            services.AddSingleton<IRepository<ProjectModel>, ProjectRepository>();
            services.AddSingleton<UserService>();
            services.AddSingleton<TeamService>();
            services.AddSingleton<TaskService>();
            services.AddSingleton<ProjectService>();
            services.AddSingleton<SelectServices>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
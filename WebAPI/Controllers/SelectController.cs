﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Services;


namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SelectController : ControllerBase
    {
        private readonly SelectServices _selectServices;
        public  SelectController(SelectServices selectServices)
        {
            _selectServices = selectServices;
        }
        [HttpGet("0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetTasksEntitiesById(int id)
        {
            return Ok(_selectServices.GetTasksEntitiesById(id));
        }
        [HttpGet("1")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetLessThan45(int id)
        {
            return Ok(_selectServices.GetLessThan45(id));
        }
        [HttpGet("2")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult FinishedIn2021(int id)
        {
            return Ok(_selectServices.GetLessThan45(id));
        }
        [HttpGet("3")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult TeamsOlder10Sorted()
        {
            return Ok(_selectServices.TeamsOlder10Sorted());
        }
        [HttpGet("4")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult UsersAlphabeticByTasksLength()
        {
            return Ok(_selectServices.UsersAlphabeticByTasksLength());
        }
        [HttpGet("5")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult ComplexUserEntity(int id)
        {
            return Ok(_selectServices.ComplexUserEntity(id));
        }
        [HttpGet("6")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult OddlyProjectTaskUserCountMix()
        {
            return Ok(_selectServices.OddlyProjectTaskUserCountMix());
        }
    }
}

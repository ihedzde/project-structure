﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Services;
using Common.DTO.Task;
using System.Net;
using System.Net.Http;
using WebAPI.CustomHttpResponse;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _taskService;
        public TaskController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAllTasks()
        {
            return Ok(_taskService.GetAllTasks());
        }
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateTask(CreateTaskDTO task)
        {
            if (!ModelState.IsValid)
                return BadRequest("Model is invalid.");
            return Ok(_taskService.CreateTask(task));
        }
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateTask(TaskDTO task)
        {
            if (!ModelState.IsValid)
                return BadRequest("Model is invalid.");
            _taskService.UpdateTask(task);
            return NoContent();
        }
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult DeleteTask(int id)
        {
            _taskService.DeleteTask(id);
            return NoContent();
        }
    }
}

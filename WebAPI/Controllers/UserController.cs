﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Common.DTO.User;
using BLL.Services;
namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;
        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAllUsers()
        {
           return Ok(_userService.GetAllUsers());
        }
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateUser(CreateUserDTO user){
            if(!ModelState.IsValid)
                return BadRequest("Model is invalid.");
            return Ok(_userService.CreateUser(user));
        }
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
         public IActionResult UpdateUser(UserDTO user){
            if(!ModelState.IsValid)
                return BadRequest("Model is invalid.");
            _userService.UpdateUser(user);
            return NoContent();
        }
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
         public IActionResult DeleteUser(int id){
            _userService.DeleteUser(id);
            return NoContent();
        }
    }
}
